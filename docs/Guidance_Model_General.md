# AGSi Guidance / Models

## Models overview

### What is a model?

A **model** is a digital geometric (1D, 2D or 3D) representation of the ground.

In theory, an AGSi model can be whatever the user wants it to be.
AGSi merely provides a data transfer format that will allow users to
exchange any type of model, within reason, albeit preferably ground related.

In particular, AGSi can support models for different:

* <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#domain-of-model">domains</a> (e.g. geology, hydrogeology, geotechnical)
* <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#category-of-model">categories of model </a>(e.g.
  <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#observational-model">observational</a> or
  <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#analytical-model">analytical</a>)
* forms of geometrical representation (e.g. 3D volumes, 2D sections, borehole
  columns)

!!! Note
    <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#model">Model</a>,
    <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#domain-of-model">domain</a>,
    <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#category-of-model">category</a> and <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#type-of-model">type</a> are defined terms in an AGSi model context. Refer to the <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions">Definitions page</a> for full details.

In order to provide a sensible structure to the AGSi schema some
assumptions have been made about how models are likely to be formed and used.
The AGSi schema is based on these assumptions. Recommendations on how
to implement the schema for typical modelling scenarios are provided in the
following. It is hoped that the schema and the recommendations for implementation
will satisfy the majority of users (specifiers, modellers, model users and
software developers).

Alternative implementations of the schema may be possible.
However, users should be aware that non-standard implementations may cause
confusion and may be problematic for some software.

!!! Note
    AGS welcomes feedback on the schema and the recommended implementations.
    The schema is based on a few examples of current practice with some thought
    given as to how this may develop in the near future.
    However, it is recognised that this is a developing subject and future practice
    may evolve in different ways.

### AGSi recommended implementation - model

The AGSi recommendations regarding what constitutes a model are illustrated in this
diagram, with commentary provided below.

!!! Note
    The diagram below shows an example of the models that may be used on a project.  

![What constitutes a model image](./images/Guidance_Model_Different.png)

Each of the blue boxes represent a separate model. Models 1, 2 and 3 are
different <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#type-of-model">types of model</a>, as follows:

1. Geological model: an <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#observational-model">observational model</a>, showing interpreted 'best guess'
geometry of the geological units. Use case typically restricted to visualisation
only. This model incorporates 3D geometry, 3D cross sections and representations of the geology in exploratory holes. These different elements are further discussed in the next section.

2. Hydrogeological model: may be similar to the geological model, but optimised
for hydrogeological modelling. This may be an <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#analytical-model">analytical model</a>
intended for direct import to analysis software.

3. Geotechnical design model: an <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#analytical-model">analytical model</a> intended to be used for
analysis/design and often limited to a specific use case
e.g. limited to foundation design in the example shown.
Normally based on interpretation and simplification of the geological
(observational) model, taking account of uncertainty and the needs of the
specified use case.

Model 4 is another geotechnical design model, but for a different use case.
In this example the geometries of models 3 and 4 are different.
If the geometries were the same and the only difference between the models
was the parameters referenced, then it would be possible to use a single model
for both by utilising the 'case' concept incorporated in the Data group.
For more details of how to do this see [agsiData - Use of case](./Guidance_Data_General.md#Use-of-Case).


### AGSi recommended implementation - model with sections and/or exploratory holes

Model 1 is an example of how different types of information, or different forms
of geometric representation, can be incorporated into a model.
This particular example includes:

- (a) 3D volume model of geological units
- (b) 3D cross sections shown within the volume model
- (c) Columns representing the geology observed in boreholes  

In this case (a) would normally be considered to be the primary view of the
model, with (b) and (c) providing supporting or clarifying information.

(a) and (b) will be made up of model elements
(<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelElement">agsiModelElement</a> objects)
embedded within the parent model (<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModel">agsiModel</a>).

In AGSi there is currently no way of grouping together model elements, e.g. of 3D model elements (a) vs section elements (c) in this example.

(c) should comprise
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationColumn">agsiObservationColumn</a> objects, embeddded within an <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationExpHole">agsiObservationExpHole</a> as applicable.
These are collected together within an <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationSet">agsiObservationSet</a> which is embedded in the relevant <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModel">agsiModel</a>.


!!! Note
    In early drafts of AGSi there was a concept of 'subsets', which could be used to group together the different parts described above. Subsets have now been deleted from AGSi due to concerns about implementation, and a desire for simplification.
    However, the current version includes the (new) concept of
    <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#observation">observations</a>, using the
    <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_Intro">Observation group</a> of objects.
    This will help to separate out the exploratory holes and their geology (and any other observations) from the remainder of the model.

More than one model can be included in an
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#agsi-file">AGSi file</a>.
Therefore, in theory all of models 1 to 4 above could be included in the same file.
Project considerations, e.g. version control and document management, may determine how models are combined or split over different files.

### Model boundaries

It is possible that the geometry defined for a model may also fully
define the true extent of the model.
However, this is commonly not the case, e.g. a surface or volume may
extend beyond what is considered to be the limit of validity of the model.

The <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelBoundary">agsiModelBoundary</a> object may be used to establish a simple boundary for
the model. The boundaries that can be defined are:

* plan area boundary, assumed to have vertical sides ('cookie-cutter')
* bottom of model boundary as a horizontal plane or a surface
* top of model boundary as a horizontal plane or a surface

Use of the top boundary is optional. In most cases the ground surface
(top of uppermost layer) provides the natural top boundary of the model
and no further definition is required.
However, there may be certain situations that require a top boundary to be defined,
and some software may require it.
Users (specifiers and/or modellers) should consider whether a top boundary is required or not.

The model is  defined as the volume enclosed within the plan area boundary, above the bottom boundary and below the top boundary (where applicable).

!!! Note
    The <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelBoundary">agsiModelBoundary</a> object is primarily intended for use with 3D models.

<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelBoundary">agsiModelBoundary</a> allows two different methods of defining the plan area
boundary:

1. Simple rectangular box defined by limiting coordinates
2. Closed polygon (requires embedment of an <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryFromFile">agsiGeometryFromFile</a> object, which must reference geometry data in a
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#agsi-supporting-files">supporting file</a>)

and two different methods of defining the top and bottom boundaries:

1. Horizontal plane at a specified elevation
2. Surface (requires embedment of an
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryFromFile">agsiGeometryFromFile</a> object).

### Version history

Minor update, for v1.0.1 of standard, June 2024:

- Links to standard updated (for change of address)

First formal issue, for v1.0.0 of standard, 30/11/2022.

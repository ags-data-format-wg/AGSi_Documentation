# Guidance / Tools
## Tools overview

This section, which will hopefully grow over time, is intended to provide information on where to find useful tools related to AGSi.

Some of these tools have been created by AGS and may be hosted here on the pages that follow. Some may be external tools that we know about.

We will give priority in these listings listing to open source and free to use tools. However, we may also list commercial software that implements AGSi as we are keen to encourage this.

!!! Note
    Please get in touch with us if you wish to have a tool or software implementation listed on this page 
    (<a href="https://gitlab.com/ags-data-format-wg/agsi/AGSi_Documentation" target="_blank">via Gitlab</a> is the best way).

!!! Note
    AGS does not provide any warranty of any kind in relation to the outputs from and performance of any tool listed here. Refer to the specific licence conditions provided for each tool including those developed by or on behalf of AGS. Unless otherwise stated, licence for tools created by AGS (including AGSi team) is the
    <a href="https://www.gnu.org/licenses/lgpl-3.0.en.html" target="_blank">GNU Lesser General Public License (LGPL3)</a>.


### Creation of AGSi files

#### AGS template files

A set of AGSi template files that may be useful for assembling AGSi JSON in a text editor. Template JSON for all AGSi objects is provided.
[Full details here](./Guidance_Tools_Templates.md).

### Editing AGSi JSON files

It will sometimes be necessary to edit the JSON ASGi files directly.

Some off-the-shelf and bespoke tools are available to help editing JSON AGSi files. [Full details here](./Guidance_Tools_JSON_Editing.md).


### Validation

Work on the AGSi validator is progressing. Visit our <a href="https://gitlab.com/ags-data-format-wg/agsi/agsi-validator-library" target="_blank">Gitlab repo here</a>.


### Version history

Page added June 2024 (for v1.0.1 release).

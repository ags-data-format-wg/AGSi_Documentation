# AGSi Guidance / Data

## Data general guidance

### Properties and parameters

For the purposes of the schema, a distinction between properties and parameters has been made as follows:

#### Property

A value reported from an observation or test (directly measured, or based on interpreted measurement), as typically reported in a factual GI report. Properties are likely to be (Eurocode 7) measured or derived values.

In AGSi, properties can be reported as simple statistical summaries of values, e.g. minimum, maximum, mean, standard deviation and number of results. Alternatively, or in addition, a text based summary or description can be reported. This supports the reporting of summaries of test results commonly seen in ground investigation reports. The properties themselves may comprise only factual data, but the process of providing a summary is considered to be part of interpretation.

Alternatively individual properties can be reported, and these may also be reported against some other (independent) variable. One example would be SPT N values reported against elevation in a particular borehole.

AGSi is not intended to carry complete sets of test data as the main AGS (factual) format already does this. However, there are occasions when it is useful to incorporate selected test results in a model as
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#complementary-data">complementary data</a>.

Properties are reported using <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Data_agsiDataPropertyValue">agsiDataPropertyValue</a> objects.

#### Parameter

An interpreted single value, or a profile of values related to an independent variable, that is to be used in design and/or analysis. Parameters are most likely to be (Eurocode 7) characteristic values.

Parameters are the values or design lines for properties determined from interpretation of the  relevant data. If best practice is being followed then this should also take into account the intended usage.

Parameters are normally intended to be applied in analysis or design, either by a human (reading the report or interrogating the model) or by a machine (direct import to analysis software or indirect import via a script).

It is unlikely that the data provided as AGSi will provide all of the input required for an analysis for design. In particular there may be other parameters, correlation factors and options that need to be input separately.

Parameters are reported using <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Data_agsiDataParameterValue">agsiDataParameterValue</a> objects.


### Use of Case

All of the Data objects (except <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Data_agsiDataPropertyFromFile">agsiDataPropertyFromFile</a>)
have the optional attribute **caseID**. This is a code (or text) that identifies the use case for a property or parameter.

Examples of the use of case include:

* Different values of the same parameter corresponding to different design/analysis methods.
* Sets of parameters for sensitivity analyses.
* Subdivision of properties or parameters within a single model element (typically a geological unit) to provide values or summary data corresponding to different soil types, e.g. separate parameters values for 'clay' and 'sand' within a model element of Alluvium.
* Provision of alternative property summaries, e.g. a summary considering all data vs a summary that excludes dubious outliers. In such cases information how the summaries have been derived should be provided, e.g. in the *remarks* attributes of the value object or in the *description* and/or *remarks* attributes of the relevant <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProjectCode">agsProjectCode</a> object.

If the input is a code, this should be defined using the <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProjectCode">agsProjectCode</a> object.

*caseID* may be left blank or omitted, but the combination of *codeID* and *caseID* shall be unique within the (array of) instances of the relevant property/parameter object contained within a single parent object instance, i.e. *caseID* is required if *codeID* is duplicated.

For AGSi output intended to be used as input to analysis/design software or scripts, it will be necessary to ensure that the analysis/design software/script knows how to interpret *caseID*. Protocols for the correct use of *caseID* should be provided in the specification.

In many cases, there will be sets of data where some parameters vary according to case, and some don't.  For example, consider this scenario:

* 2 design cases: foundation design (A) and retaining wall design (B)
* the design value of Young's Modulus (`YoungsModulus`) for case A differs from that required for case B
* the design value of bulk unit weight (`UnitWeightBulk`) is the same in both cases

There are two possible approaches to use of case in this scenario:

* `YoungsModulus` defined twice with different *caseID* used, but `UnitWeightBulk` defined once with *caseID* left blank (or use a 'default' *caseID*)
* Both `YoungsModulus` and `UnitWeightBulk` defined twice with different *caseID* (the values for both cases of `UnitWeightBulk` would be the same)

AGSi adopts a neutral stance on which is the best approach. It is up to the user(s) to define data requirements for their project in the project specification.


### Version history

Minor update, for v1.0.1 of standard, June 2024:

- Links to standard updated (for change of address)

First formal issue, for v1.0.0 of standard, 30/11/2022.

# Guidance / Tools
## AGSi JSON Editing

### Editing AGSi JSON files

### Overview

| What is it? | A User Defined Language file to highlight AGSi JSON files in Notepad++.|
| --------- | |
| Who is it by? | Amageo Ltd |
| Is it free? | Yes |
| Licence | Open source, <a href="https://www.gnu.org/licenses/lgpl-3.0.en.html" target="_blank">LGPL3</a> |
| Source code available? | Yes. See below. |
| Full information | Below |


### Introduction

Almost any text editors (not word processors) can edit JSON files. The most commonly used cross-platform* editors are 
<a href="https://www.sublimetext.com" target="_blank">Sublime Text</a>,
<a href="https://notepad-plus-plus.org" target="_blank">Notepad++</a> and 
<a href="https://code.visualstudio.com" target="_blank">Visual Studio Code</a>.
The use of MS Windows Notepad is limited and not recommended for anything other than a quick edit.

The editors listed above have in-built JSON tools and also plugins or extensions that can help visualise the data or provide snippet tools that can insert pre-formatted text.

The following is a list of some useful plugins/extensions.


|Editor|Plugin/extension|
|------|------------------|
|Notepad++|JSTool|
|Sublime Text|Json Colors and Navigation|
|Visual Studio Code|json|

*(Windows & Linux) Notepad++ may need an emulator in Linux

### Language highlighting

Most text editors also provide language syntax highlighting for keywords, data types and operators in common languages, such as Python, C and Visual Basic. This can be useful when reading the code or spotting errors.

A language syntax highlighter for AGSi has been created for Notepad++ as shown below. It highlights the keywords used in AGSi and fades out the double quotes so that the data/values are easier to see. Misspelt keywords lose their highlighting, so it is immediately obvious is there's a keyword error.

This is not a substitute for full validation but it may be helpful in identifying some potential errors at source.

![AGSi language syntax in notepad++](images/agsi-language-syntax.png "AGSi language syntax in notepad++")

The User Defined Language (xml) file can be downloaded from
<a href="https://gitlab.com/amageo/ags-tools/-/snippets/3719264" target="_blank">here</a>,
instructions of how to use the file are provided at the download.

The User Defined file will be updated to reflect the current version of the standard.



### Version history

Page added June 2024 (for v1.0.1 release).

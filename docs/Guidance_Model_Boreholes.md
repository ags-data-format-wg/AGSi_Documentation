# AGSi Guidance / Models

## Models showing exploratory holes

It is common for models to incorporate representations of exploratory holes and the
geological units encountered within them, typically displayed as cylinders with
different segments coloured to represent different geology.
One of the benefits is this provides a visual indication of the
data used to derive the model.

In AGSi, the information provided directly from exploratory holes is classed as
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#observation">observations</a>
because it is predominantly
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#factual-data">factual data</a>
albeit in some cases there may have been some
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#interpreted-data">interpretation</a>
applied (this subject is further discussed [below](#factual-vs-interpreted-data)).

!!! Note
    Some users may consider exploratory hole information to be part of the model. Some may consider it not part of the model as such, but supporting data. For AGSi it does not matter! In AGSi observations are contained within an AGSi model object, but that is just how the schema happens to be structured.

![3D view of exploratory holes](./images/Guidance_Model_Boreholes_General.png)

The section discusses how exploratory holes can be handled within AGSi.

The term **hole** is often used in this section in the interests of brevity to
represent any type of exploratory hole including boreholes, coreholes, trial pits, CPT etc.

### Modelling exploratory holes - principles

#### Overview

In AGSi, <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_Intro">Observation group</a> objects are used to represent:

- exploratory holes (including selected metadata)
- the geology found within the exploratory holes (and/or some other type of classification, if preferred)
- if required, selected test data or other information from exploratory holes

Individual observation objects are used for the above. These objects are then grouped into **sets** of observations. It is the sets of observations that are then incorporated into the relevant model.

In AGSi hole information is transferred as data, not as the geometry of the lines/cylinders created by the modelling software.

!!! Note
    This approach has been adopted as the authors believe that it reflects the current reality of modelling software in the ground domain. Such software typically ingests and stores exploratory hole information as 'data', often in a format close to the original source AGS data. The visualisation of that data (lines/cylinders) is generated internally from the data. The software often allows customisation of the visual representations, e.g. size of cylinder, colours and symbology.

    The flipside is that software not adopting this methodology, e.g. not geared towards visualisation of the ground, may not be able to deal with this data without some intervention, e.g. pre-processing of the data or extension of the software.

The primary focus of AGSi is ground models and interpreted data. AGSi is not intended to replace or replicate the
<a href="https://www.ags.org.uk/data-format/" target="_blank">existing AGS format</a>
for transfer of factual data from ground investigations (GI), or other established formats for transfer of factual data.  It is recommended that AGSi should only be used to transfer modest amounts of test data, and only when required. Such data should add value to the model, taking into account its intended usage.

The <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_Intro">Observation group</a> can be used for much more than just exploratory holes, but that is beyond the scope of this page.


#### Exploratory holes, investigations and sets

If data from exploratory holes is to be provided using AGSi, then the holes themselves must be included. AGSi includes an object specifically designed for this
(<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationExpHole">agsiObservationExpHole</a>).
Location and depth must be provided as a minimum. For holes that are inclined or not straight, a profile (set of coordinates) can be provided instead. Attributes are provided for basic metadata such as hole name, type and date. If further data needs to be provided, this can be done by embedding data objects (see [below](#including-test-results)).

In AGSi, all individual observation objects (such as an exploratory hole) must be included in an **observation set** because it is the sets that are incorporated into relevant model. This applies even if there is only one observation!

A model can have more than one set of observations.

For holes it makes sense for the sets to correspond to the source
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#ground-investigation">ground investigations</a>.
Detailed metadata for a ground investigation should be provided using
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProjectInvestigation">agsProjectInvestigation</a>
in the <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_Intro">Project group</a>.
The observation set object conveniently includes an attribute that can reference the <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProjectInvestigation">agsProjectInvestigation</a> object.

Alignment of observation sets to ground investigations is not mandatory. Users can adopt an alternative system if they wish.

!!! Note
    If you have observations that are relevant to more than one model, these will need to be provided (embedded in parent model) separately for each model.

    An alternative solution could be to provide the exploratory holes only in their own 'model', leaving it to the end user to combine the relevant models downstream, assuming they are able to do so. However, user agreement should be sought before adopting such an approach.

#### Geology in exploratory holes

In AGSi, the geological succession in a hole, i.e. the 'cylinders', is defined using the **column** object
(<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationColumn">agsiObservationColumn</a>) that is specifically designed for this purpose.

In each hole, each 'segment' in the geological succession (column) requires its own column (segment) object.

!!! Note
    Exploratory hole logs often have adjacent segments that have the same geological classification, differing only by description. This is perfectly acceptable in AGSi. Adjacent segments can be of the same geological unit if necessary. If such segments have been consolidated into one as part of the modelling process (some software allows this) then that is also acceptable as far as AGSi is concerned.

In AGSi, the column (segment) objects must be contained within a parent hole
(<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationExpHole">agsiObservationExpHole</a> object).
The columns inherit their plan (XY) location and top of hole elevation (Z), or alignment profile if applicable, from the parent hole.

The vertical extents of each column (segment) can be defined by reference to elevation or depth below the top of the parent hole, or both. If both are defined, then it is up to the user to ensure that the data provided is compatible. If the data is incompatible, then it is up to the user(s) to resolve that.

!!! Note
    At present, no recommendation is made as to whether to use depth, elevation or both. We will let industry evolve best practice and may incorporate that in a future version.

For inclined holes, or holes that are not straight (defined using a profile) **depth** is defined as the **length** measured along the line of the hole, NOT the vertical depth. Therefore care must be taken to ensure that the correct 'depth' is provided. Elevation is always elevation (Z coordinate).

For horizontal holes, depth (as length) will need to be used.

!!! Note
    This definition of depth is the same as that used in the AGS (factual) data format.

The column (segment) object includes attributes that replicate the AGS (factual) data format GEOL group, i.e. description, legend code, geology code etc.

The column object may be used to provide other types of data that use the depth/elevation from/to (range) structure. However, it will be necessary to embed an object from the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Data_Intro">Data group</a> to do this.

For data that corresponds to a discrete depth/elevation, use of the methodology described for test data [below](#including-test-results) is preferred.  

An overview of the relationships between sets (investigations), holes and column (segments) is provided below.

![Borehole geology segment sets](./images/Guidance_Model_Boreholes_Concept.png)

#### Factual vs interpreted data?

!!! Note
    It is recognised that the boundary between
    <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#factual-data">factual data</a> and
    <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#interpreted-data">interpreted data</a>
    in ground engineering is often a blurry one, interpreted in different ways by different people.

    The AGSi schema does not care whether something is factual, interpreted, or something between. However, if there is potential for any uncertainty about the origin and nature of the data provided, then it is strongly recommended that information about this be provided, preferably within the data as metadata, so that end users are at least kept informed.

    The following is an example of where confusion can arise, and how this can be resolved.

It will commonly be the case that exploratory hole geological classifications and descriptions
shared using AGSi will be the same as those found in the factual data, i.e. same as on the borehole logs.
However, this does not need to be the case, and in some cases perhaps should not be the case. Examples include:

* Re-interpretation of geological classification where original classification in factual data was ambiguous or disputed
* Updated geological classifications for historic or legacy data
* Consolidation of adjacent log segments with same classification into a single segment per unit
* Alternative simplified (interpreted) descriptions provided

Furthermore, the AGS (factual) format GEOL group data typically used to provide the descriptions in a 'model' does not necessarily provide the full
information provided by an exploratory hole log from an investigation report. Logs in reports generally include data from other source AGS factual groups, such as geological detailed descriptions (DETL group), groundwater observations and other depth related remarks.

Experienced practitioners from the ground domain may recognise that the exploratory hole information provided in a model does not replicate the full report log information, and will treat it accordingly. However, to be on the safe side it is recommended that any omissions, modifications, simplifications etc. applicable to the exploratory hole data provided in AGSi should be flagged up, preferably within the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#agsi-package">AGSi package</a>.
A brief summary should be included in the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#agsi-file">AGSi file</a> itself (use of
*<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationSet#remarks">remarks</a>* attribute in
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationSet">agsiObservationSet</a>
is recommended) with full details provided in supporting
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#agsi-document">documents</a>
if necessary.

#### Including test results

We reiterate that AGSi is not intended to carry the full factual GI data, or large parts of it. Please continue to use the <a href="https://www.ags.org.uk/data-format/" target="_blank">AGS (factual) data transfer format</a> for that.
However, inclusion of the relevant (factual) AGS file(s) as
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#agsi-document">documents</a> within the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#agsi-package">AGSi package</a>
is acceptable (and encouraged).

It is recognised that some models incorporate selected test data, e.g. SPT N values in for each hole, and that users may wish to include such data as part of the model to be shared.
AGSi addresses this use case.

!!! Note
    This example should not be taken as encouragement to include SPT data in a model. If there are good reasons for doing so, and it is agreed that it should be included, then ok. But if only SPTs are included, and not other test data that is just as important, then it should be questioned.

    Caution regarding inclusion of test data arises from the following concerns:

    - Desire to avoid duplication of factual data
    - Data brought into models is often incomplete, lacking metadata and context
    - Some data brought into models may have undergone some interpretation, e.g. extrapolation of SPT N values, details of which may not be immediately apparent

    It is anticipated that best practice on inclusion of test data, or not as the case may be, will evolve over time, and future guidance will be able to reflect this.

If test data is to be included, it is recommended that the data for each hole is provided as a profile of values against depth or elevation. The profile is defined using
an object from the <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Data_Intro">Data group</a> which gets embedded into the parent hole object.

This method allows for multiple sets of test data (different tests) to be included, if necessary.

Additional hole data, i.e. not depth related, can also be accommodated in a similar manner, the only difference being that a single value (numeric or text) would be provided, not a profile.

!!! Note
    In theory it is possible to provide test data via the column (segment) object, which would allow an individual test result to be provided each time. This method is not recommended.


### Modelling exploratory holes - use of schema

The exploratory hole representations are
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#observation">observations</a>
that are 'modelled' using objects from the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_Intro">Observation group</a>.

These observation objects (described below) are collected into sets that are defined using
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationSet">agsiObservationSet</a>.
This object is primarily a means of grouping together individual observations (exploratory holes in this case) but simple metadata for the 'set' can be provided too. In particular, if the set corresponds to a <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#ground-investigation">ground investigation</a>,
the <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationSet#investigationid">*investigationID*</a>
attribute can be used to reference the ID of an
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProjectInvestigation">agsProjectInvestigation</a>
object providing the detailed metadata for the investigation (if used).

The <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationSet">agsiObservationSet</a>
objects are themselves embedded in the relevant parent
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModel">agsiModel</a> object.
More than one set can be included in a model, which allows flexibility on how the sets are formed.

Each individual hole is described using an
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationExpHole">agsiObservationExpHole</a> object.
This includes the location and depth (or alignment) data for the hole as well as some limited
metadata. If further data is required to be associated with the hole then
this may be achieved by embedding one or more
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Data_agsiDataPropertyValue">agsiDataPropertyValue</a>
objects under the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationExpHole#agsidatapropertyvalue">*agsiDataPropertyValue*</a>
attribute (see [Guidance / Data](./Guidance_Data_General.md) for more details on how to do this).

The geological succession in each hole is defined by providing an
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationColumn">agsiObservationColumn</a> object
for each 'segment'.
These are embedded within the parent
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationExpHole">agsiObservationExpHole</a>
as an array under the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationExpHole#agsiobservationcolumn">*agsiObservationColumn*</a>
attribute.

A simple example illustrating these concepts is given
[on the next page](./Guidance_Model_Boreholes_Example.md).

A diagram showing the above objects and their relationships is given below.

<a href="../images/Guidance_Obs_ExpHoles.svg" target="_blank">Click here to a open full size version of this diagram in a new browser window</a>.

!!! Note
    This diagram includes only the objects mentioned above. Relationships of these objects to other objects in the schema are not shown.

![Exploratory holes schema extract](./images/Guidance_Obs_ExpHoles.svg)

### Version history

Minor update, for v1.0.1 of standard, June 2024:

- Links to standard updated (for change of address)

First formal issue, for v1.0.0 of standard, 30/11/2022.

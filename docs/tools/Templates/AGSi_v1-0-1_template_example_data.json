{
    "agsSchema": {
        "name": "AGSi",
        "version": "1.0.1",
        "link": "https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/1.0.1/"
    },
    "agsFile": {
        "fileUUID": "98e17952-c99d-4d87-8f01-8ba75d29b6ad",
        "title": "Stage 3 Sitewide Ground models",
        "projectTitle": "Gotham City Metro Purple Line, C999 Geotechnical Package X",
        "description": "Geological model and geotechnical design models produced for Stage 4 design",
        "producedBy": "ABC Consultants",
        "fileURI": "https://gothammetro.sharepoint.com/C999/docs/C999-ABC-AX-XX-M3-CG-1234",
        "reference": "C999-ABC-AX-XX-M3-CG-1234",
        "revision": "P1",
        "date": "2018-10-05",
        "status": "Final",
        "statusCode": "S2",
        "madeBy": "A Green",
        "checkedBy": "P Brown",
        "approvedBy": "T Black",
        "remarks": "Some remarks if required"
    },
    "agsProject": {
        "projectUUID": "f7884d64-77ae-4eaf-b223-13c21bc2504b",
        "projectName": "C999 Geotechnical Package X",
        "producer": "ABC Consultants",
        "producerSuppliers": "Acme Environmental, AN Other Organisation",
        "client": "XYZ D&B Contractor",
        "description": "Stage 3 sitewide ground modelling, including incorporation of new 2018 GI data.",
        "projectCountry": "United Kingdom",
        "producerProjectID": "P12345",
        "clientProjectID": "C999/ABC",
        "parentProjectName": "C999 Area A Phase 1 Design and Build",
        "ultimateProjectName": "Gotham City Metro Purple Line",
        "ultimateProjectClient": "City Transport Authority",
        "briefDocumentSetID": "ExampleDocSetID",
        "reportDocumentSetID": "ExampleDocSetID",
        "agsProjectCoordinateSystem": [
            {
                "systemID": "MetroXYZ",
                "description": "3D model coordinate system: Gotham Metro Grid + OS elevations",
                "systemType": "XYZ",
                "systemNameXY": "Gotham Metro Grid",
                "systemNameZ": "Ordnance Datum Newlyn",
                "axisNameX": "Easting",
                "axisNameY": "Northing",
                "axisNameZ": "Elevation",
                "axisUnitsXY": "m",
                "axisUnitsZ": "mOD",
                "globalXYSystem": "British National Grid",
                "globalZSystem": "Ordnance Datum Newlyn",
                "transformShiftX": 450000,
                "transformShiftY": 125000,
                "transformXYRotation": 1.44450116,
                "transformXYScaleFactor": 0.9999745653,
                "transformShiftZ": -100,
                "remarks": "Some remarks if required"
            }
        ],
        "agsProjectInvestigation": [
            {
                "investigationID": "GIPackageA",
                "investigationName": "Gotham City Metro Purple Line, C999 Package A",
                "description": "Preliminary sitewide investigation, March-July 2018",
                "contractor": "Boring Drilling Ltd",
                "client": "XYZ D&B Contractor",
                "engineer": "ABC Consultants",
                "parentProjectName": "C999 Area A Phase 1 Design and Build",
                "ultimateProjectName": "Gotham City Metro Purple Line",
                "ultimateProjectClient": "City Transport Authority",
                "subcontractors": "Acme Specialist Lab, XYZ Environmental Lab",
                "fieldworkDateStart": "2018-08-21",
                "scopeDescription": "Preliminary investigation comprising 27 boreholes of which 15 extended by rotary coring up to 55m depth, 45 CPT (max 15m), 35 trial pits, 26 dynamic sampling holes, geotechnical and contamination sampling and testing, piezometric monitoring, limited gas monitoring.",
                "locationCoordinateProject": [
                    25500.0,
                    13200.0
                ],
                "locationCoordinateGlobal": [
                    475270.0,
                    137965.0
                ],
                "locationDescription": "Gotham City, west, central and southeast",
                "specificationDocumentSetID": "ExampleDocSetID",
                "reportDocumentSetID": "ExampleDocSetID",
                "dataDocumentSetID": "ExampleDocSetID",
                "agsiDataPropertyValue": [
                    {
                        "dataID": "42f18976-7352-4f67-9a6e-df89788343a7",
                        "codeID": "TRIG_CU",
                        "caseID": "Clay",
                        "valueNumeric": 120,
                        "valueText": "Dry",
                        "valueProfileIndVarCodeID": "Elevation",
                        "valueProfile": [
                            [
                                15.5,
                                60.0
                            ],
                            [
                                14.0,
                                75.0
                            ],
                            [
                                12.5,
                                105.0
                            ]
                        ],
                        "remarks": "Some remarks if required"
                    }
                ],
                "agsiDataPropertyFromFile": [
                    {
                        "dataID": "42f18976-7352-4f67-9a6e-df89788343a7",
                        "description": "Additional data points for top of Gotham Clay from legacy boreholes, based on points marked on plan reference xxxx in report yyyy etc",
                        "fileFormat": "XLSX",
                        "fileFormatVersion": "2019",
                        "fileURI": "data/geology/legacydata.xlsx",
                        "filePart": "GothamClay",
                        "revision": "P3",
                        "date": "2018-10-01",
                        "revisionInfo": "Minor corrections, updated for GIR rev P2.",
                        "remarks": "Some remarks if required"
                    }
                ],
                "remarks": "Some additional remarks"
            }
        ],
        "agsProjectDocumentSet": [
            {
                "documentSetID": "ExampleDocSetID",
                "description": "Package A factual report",
                "agsProjectDocument": [
                    {
                        "documentID": "eac20ae4-25a1-4e68-96f8-cf43b9761b11",
                        "title": "Factual ground investigation report, Gotham City Metro Purple Line, C999 Package A, Volume 1 of 3",
                        "description": "Package A factual report, Volume 1",
                        "author": "Boring Drilling Ltd",
                        "client": "XYZ D&B Contractor",
                        "originalReference": "12345/GI/2",
                        "systemReference": "C999-BDL-AX-XX-RP-WG-0002",
                        "revision": "P2",
                        "date": "2018-09-06",
                        "status": "Final",
                        "statusCode": "S2",
                        "documentURI": "docs/GI/C999-BDL-AX-XX-RP-WG-0002.pdf",
                        "documentSystemURI": "https://gothammetro.sharepoint.com/C999/docs/C999-BDL-AX-XX-RP-WG-0002",
                        "remarks": "Some additional remarks"
                    }
                ],
                "remarks": "Some additional remarks"
            }
        ],
        "agsProjectCodeSet": [
            {
                "codeSetID": "CodeSetParameter",
                "description": "Parameter codes",
                "usedByObject": "agsiDataParameterValue",
                "usedByAttribute": "codeID",
                "sourceDescription": "AGSi standard code list",
                "sourceURI": "https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/1.0.1/Codes_Codelist/",
                "concatenationAllow": false,
                "concatenationCharacter": "+",
                "agsProjectCode": [
                    {
                        "codeID": "UndrainedShearStrength",
                        "description": "Undrained shear strength",
                        "units": "kPa",
                        "isStandard": true,
                        "remarks": "Some additional remarks"
                    }
                ],
                "remarks": "Some additional remarks"
            }
        ],
        "remarks": "Some remarks if required"
    },
    "agsiModel": [
        {
            "modelID": "1fb599ab-c040-408d-aba0-85b18bb506c2",
            "modelName": "Sitewide geological model",
            "description": "C999 Package X Sitewide geological model exported from SomeGeoModelSoftware. Incorporates 2018 GI data ",
            "coordSystemID": "MetroXYZ",
            "modelType": "Geological model",
            "category": "Observational",
            "domain": "Engineering geology",
            "input": "Input data described in GIR section 3.3.2",
            "method": "3D model created in SomeGeoModelSoftware. See GIR section 3.3.3 for details.",
            "usage": "Observational and interpolated geological profile. For reference and visualisation only. Not suitable for direct use in design. See GIR section 3.3.4 for details.",
            "uncertainty": "The boundaries of the geological units presented in this model are a best estimate based on interpolation between exploratory holes, which in some cases are >100m apart. In addition, in some places the boundaries are based on interpretation of CPT results. Therefore the unit boundaries shown are subject to uncertainty, which increases with distance from the exploratory holes. Refer to GIR section 3.3.4 for more information. ",
            "documentSetID": "ExampleDocSetID",
            "alignmentID": "sectionAA",
            "agsiModelElement": [
                {
                    "elementID": "GC/W",
                    "elementName": "Gotham Clay, west zone",
                    "description": "Stiff to very stiff slightly sandy blue/grey CLAY, with occasional claystone layers (typically <0.1m). Becoming very sandy towards base of unit.",
                    "elementType": "Geological unit",
                    "geometryObject": "agsiGeometryVolFromSurfaces",
                    "agsiGeometry": {
                        "geometryID": "GeologyGCC",
                        "description": "Gotham Clay",
                        "agsiGeometryTop": {
                            "geometryID": "GeologyGCCTop",
                            "description": "Top of GC",
                            "geometryType": "Surface",
                            "fileFormat": "LandXML",
                            "fileFormatVersion": "2.0",
                            "fileURI": "geometry/geology/GCtop.xml",
                            "filePart": "GCTop",
                            "revision": "P2",
                            "date": "2018-10-07",
                            "revisionInfo": "Updated for GIR rev P2. Additional BH from 2018 GI included.",
                            "remarks": "Some remarks if required"
                        },
                        "agsiGeometryBottom": {
                            "geometryID": "GeologyGCCTop",
                            "description": "Top of GC",
                            "geometryType": "Surface",
                            "fileFormat": "LandXML",
                            "fileFormatVersion": "2.0",
                            "fileURI": "geometry/geology/GCtop.xml",
                            "filePart": "GCTop",
                            "revision": "P2",
                            "date": "2018-10-07",
                            "revisionInfo": "Updated for GIR rev P2. Additional BH from 2018 GI included.",
                            "remarks": "Some remarks if required"
                        },
                        "remarks": "Some remarks if required"
                    },
                    "agsiGeometryAreaLimit": {
                        "geometryID": "GeologyGCCTop",
                        "description": "Top of GC",
                        "geometryType": "Surface",
                        "fileFormat": "LandXML",
                        "fileFormatVersion": "2.0",
                        "fileURI": "geometry/geology/GCtop.xml",
                        "filePart": "GCTop",
                        "revision": "P2",
                        "date": "2018-10-07",
                        "revisionInfo": "Updated for GIR rev P2. Additional BH from 2018 GI included.",
                        "remarks": "Some remarks if required"
                    },
                    "agsiDataParameterValue": [
                        {
                            "dataID": "42f18976-7352-4f67-9a6e-df89788343a7",
                            "codeID": "UndrainedShearStrength",
                            "caseID": "EC7Pile",
                            "valueNumeric": 75,
                            "valueText": "100 + 6z (z=0 @ +6.0mOD)",
                            "valueProfileIndVarCodeID": "Elevation",
                            "valueProfile": [
                                [
                                    6.0,
                                    100.0
                                ],
                                [
                                    -24.0,
                                    280.0
                                ]
                            ],
                            "remarks": "Some remarks if required"
                        }
                    ],
                    "agsiDataPropertyValue": [
                        {
                            "dataID": "42f18976-7352-4f67-9a6e-df89788343a7",
                            "codeID": "TRIG_CU",
                            "caseID": "Clay",
                            "valueNumeric": 120,
                            "valueText": "Dry",
                            "valueProfileIndVarCodeID": "Elevation",
                            "valueProfile": [
                                [
                                    15.5,
                                    60.0
                                ],
                                [
                                    14.0,
                                    75.0
                                ],
                                [
                                    12.5,
                                    105.0
                                ]
                            ],
                            "remarks": "Some remarks if required"
                        }
                    ],
                    "agsiDataPropertySummary": [
                        {
                            "dataID": "42f18976-7352-4f67-9a6e-df89788343a7",
                            "codeID": "TRIG_CU",
                            "caseID": "Clay",
                            "valueMin": 78,
                            "valueMax": 345,
                            "valueMean": 178.2,
                            "valueStdDev": 36.4,
                            "valueCount": 58,
                            "valueSummaryText": "<0.01 to 12.57, mean 3.21, (16 results)",
                            "remarks": "Some remarks if required"
                        }
                    ],
                    "agsiDataPropertyFromFile": {
                        "dataID": "42f18976-7352-4f67-9a6e-df89788343a7",
                        "description": "Additional data points for top of Gotham Clay from legacy boreholes, based on points marked on plan reference xxxx in report yyyy etc",
                        "fileFormat": "XLSX",
                        "fileFormatVersion": "2019",
                        "fileURI": "data/geology/legacydata.xlsx",
                        "filePart": "GothamClay",
                        "revision": "P3",
                        "date": "2018-10-01",
                        "revisionInfo": "Minor corrections, updated for GIR rev P2.",
                        "remarks": "Some remarks if required"
                    },
                    "colourRGB": "#c0c0c0",
                    "remarks": "Some additional remarks"
                }
            ],
            "agsiModelBoundary": {
                "boundaryID": "BoundarySitewide",
                "description": "Boundary for Geological Model: sitewide",
                "minX": 20000,
                "maxX": 35000,
                "minY": 10000,
                "maxY": 15000,
                "topElevation": 40,
                "bottomElevation": -40,
                "agsiGeometryBoundaryXY": {
                    "geometryID": "GeologyGCCTop",
                    "description": "Top of GC",
                    "geometryType": "Surface",
                    "fileFormat": "LandXML",
                    "fileFormatVersion": "2.0",
                    "fileURI": "geometry/geology/GCtop.xml",
                    "filePart": "GCTop",
                    "revision": "P2",
                    "date": "2018-10-07",
                    "revisionInfo": "Updated for GIR rev P2. Additional BH from 2018 GI included.",
                    "remarks": "Some remarks if required"
                },
                "agsiGeometrySurfaceTop": {
                    "geometryID": "GeologyGCCTop",
                    "description": "Top of GC",
                    "geometryType": "Surface",
                    "fileFormat": "LandXML",
                    "fileFormatVersion": "2.0",
                    "fileURI": "geometry/geology/GCtop.xml",
                    "filePart": "GCTop",
                    "revision": "P2",
                    "date": "2018-10-07",
                    "revisionInfo": "Updated for GIR rev P2. Additional BH from 2018 GI included.",
                    "remarks": "Some remarks if required"
                },
                "agsiGeometrySurfaceBottom": {
                    "geometryID": "GeologyGCCTop",
                    "description": "Top of GC",
                    "geometryType": "Surface",
                    "fileFormat": "LandXML",
                    "fileFormatVersion": "2.0",
                    "fileURI": "geometry/geology/GCtop.xml",
                    "filePart": "GCTop",
                    "revision": "P2",
                    "date": "2018-10-07",
                    "revisionInfo": "Updated for GIR rev P2. Additional BH from 2018 GI included.",
                    "remarks": "Some remarks if required"
                },
                "remarks": "Some additional remarks"
            },
            "agsiModelAlignment": [
                {
                    "alignmentID": "sectionAA",
                    "alignmentName": "Section AA",
                    "description": "East-west section through site",
                    "agsiGeometry": {
                        "geometryID": "GeologyGCCTop",
                        "description": "Top of GC",
                        "geometryType": "Surface",
                        "fileFormat": "LandXML",
                        "fileFormatVersion": "2.0",
                        "fileURI": "geometry/geology/GCtop.xml",
                        "filePart": "GCTop",
                        "revision": "P2",
                        "date": "2018-10-07",
                        "revisionInfo": "Updated for GIR rev P2. Additional BH from 2018 GI included.",
                        "remarks": "Some remarks if required"
                    },
                    "startChainage": 1000,
                    "remarks": "Some additional remarks"
                }
            ],
            "agsiObservationSet": [
                {
                    "observationSetID": "Obs/GIHolesA",
                    "description": "2018 GI Package A",
                    "investigationID": "GIPackageA",
                    "documentSetID": "ExampleDocSetID",
                    "agsiObservationExpHole": [
                        {
                            "holeID": "A/BH01",
                            "holeUUID": "523ad9ed-4f75-4a55-b251-c566a8b998bd",
                            "holeName": "BH01",
                            "topCoordinate": [
                                1275.5,
                                2195.0,
                                15.25
                            ],
                            "verticalHoleDepth": 25,
                            "profileCoordinates": [
                                [
                                    1275.5,
                                    2195.0,
                                    15.25
                                ],
                                [
                                    1275.5,
                                    2195.0,
                                    -9.75
                                ]
                            ],
                            "holeType": "CP+RC",
                            "date": "2018-05-23",
                            "agsiObservationColumn": [
                                {
                                    "columnID": "8526ef28-7a26-4c6f-b305-5e9607a7ab6d",
                                    "topDepth": 8.9,
                                    "bottomDepth": 34.7,
                                    "topElevation": 6.3,
                                    "bottomElevation": -28.4,
                                    "description": "Stiff to very stiff blue grey slightly sandy silty CLAY with rare claystone layers (GOTHAM CLAY)",
                                    "legendCode": "201",
                                    "geologyCode": "GC",
                                    "geologyCode2": "A2",
                                    "geologyFormation": "Gotham Clay",
                                    "geologyBGS": "GC",
                                    "agsiDataPropertyValue": [
                                        {
                                            "dataID": "42f18976-7352-4f67-9a6e-df89788343a7",
                                            "codeID": "TRIG_CU",
                                            "caseID": "Clay",
                                            "valueNumeric": 120,
                                            "valueText": "Dry",
                                            "valueProfileIndVarCodeID": "Elevation",
                                            "valueProfile": [
                                                [
                                                    15.5,
                                                    60.0
                                                ],
                                                [
                                                    14.0,
                                                    75.0
                                                ],
                                                [
                                                    12.5,
                                                    105.0
                                                ]
                                            ],
                                            "remarks": "Some remarks if required"
                                        }
                                    ],
                                    "remarks": "Some remarks if required"
                                }
                            ],
                            "agsiDataPropertyValue": [
                                {
                                    "dataID": "42f18976-7352-4f67-9a6e-df89788343a7",
                                    "codeID": "TRIG_CU",
                                    "caseID": "Clay",
                                    "valueNumeric": 120,
                                    "valueText": "Dry",
                                    "valueProfileIndVarCodeID": "Elevation",
                                    "valueProfile": [
                                        [
                                            15.5,
                                            60.0
                                        ],
                                        [
                                            14.0,
                                            75.0
                                        ],
                                        [
                                            12.5,
                                            105.0
                                        ]
                                    ],
                                    "remarks": "Some remarks if required"
                                }
                            ],
                            "agsiDataPropertyFromFile": {
                                "dataID": "42f18976-7352-4f67-9a6e-df89788343a7",
                                "description": "Additional data points for top of Gotham Clay from legacy boreholes, based on points marked on plan reference xxxx in report yyyy etc",
                                "fileFormat": "XLSX",
                                "fileFormatVersion": "2019",
                                "fileURI": "data/geology/legacydata.xlsx",
                                "filePart": "GothamClay",
                                "revision": "P3",
                                "date": "2018-10-01",
                                "revisionInfo": "Minor corrections, updated for GIR rev P2.",
                                "remarks": "Some remarks if required"
                            },
                            "remarks": "Original name on logs: BH1"
                        }
                    ],
                    "agsiObservationPoint": [
                        {
                            "observationID": "GIHole/A/Obs/001",
                            "observationNature": "GI field notes",
                            "coordinate": [
                                1280.0,
                                2195.0
                            ],
                            "madeBy": "J Smith (ABC Consultants)",
                            "date": "2018-05-18",
                            "observationText": "Original proposed location of BH01.  Hole moved due to concrete obstruction encountered at this location. ",
                            "agsiDataPropertyValue": [
                                {
                                    "dataID": "42f18976-7352-4f67-9a6e-df89788343a7",
                                    "codeID": "TRIG_CU",
                                    "caseID": "Clay",
                                    "valueNumeric": 120,
                                    "valueText": "Dry",
                                    "valueProfileIndVarCodeID": "Elevation",
                                    "valueProfile": [
                                        [
                                            15.5,
                                            60.0
                                        ],
                                        [
                                            14.0,
                                            75.0
                                        ],
                                        [
                                            12.5,
                                            105.0
                                        ]
                                    ],
                                    "remarks": "Some remarks if required"
                                }
                            ],
                            "agsiDataPropertyFromFile": {
                                "dataID": "42f18976-7352-4f67-9a6e-df89788343a7",
                                "description": "Additional data points for top of Gotham Clay from legacy boreholes, based on points marked on plan reference xxxx in report yyyy etc",
                                "fileFormat": "XLSX",
                                "fileFormatVersion": "2019",
                                "fileURI": "data/geology/legacydata.xlsx",
                                "filePart": "GothamClay",
                                "revision": "P3",
                                "date": "2018-10-01",
                                "revisionInfo": "Minor corrections, updated for GIR rev P2.",
                                "remarks": "Some remarks if required"
                            },
                            "remarks": "Some remarks if required"
                        }
                    ],
                    "agsiObservationShape": [
                        {
                            "observationID": "FieldGeology/Obs/001",
                            "observationNature": "Geological field survey of visible outcrop",
                            "madeBy": "S Jones (ABC Consultants)",
                            "date": "2018-03-13",
                            "observationText": "Surveyed boundary of top of Gotham Clay, in side of exposed cutting.",
                            "agsiGeometryFromFile": {
                                "geometryID": "GeologyGCCTop",
                                "description": "Top of GC",
                                "geometryType": "Surface",
                                "fileFormat": "LandXML",
                                "fileFormatVersion": "2.0",
                                "fileURI": "geometry/geology/GCtop.xml",
                                "filePart": "GCTop",
                                "revision": "P2",
                                "date": "2018-10-07",
                                "revisionInfo": "Updated for GIR rev P2. Additional BH from 2018 GI included.",
                                "remarks": "Some remarks if required"
                            },
                            "agsiDataPropertyValue": [
                                {
                                    "dataID": "42f18976-7352-4f67-9a6e-df89788343a7",
                                    "codeID": "TRIG_CU",
                                    "caseID": "Clay",
                                    "valueNumeric": 120,
                                    "valueText": "Dry",
                                    "valueProfileIndVarCodeID": "Elevation",
                                    "valueProfile": [
                                        [
                                            15.5,
                                            60.0
                                        ],
                                        [
                                            14.0,
                                            75.0
                                        ],
                                        [
                                            12.5,
                                            105.0
                                        ]
                                    ],
                                    "remarks": "Some remarks if required"
                                }
                            ],
                            "agsiDataPropertyFromFile": {
                                "dataID": "42f18976-7352-4f67-9a6e-df89788343a7",
                                "description": "Additional data points for top of Gotham Clay from legacy boreholes, based on points marked on plan reference xxxx in report yyyy etc",
                                "fileFormat": "XLSX",
                                "fileFormatVersion": "2019",
                                "fileURI": "data/geology/legacydata.xlsx",
                                "filePart": "GothamClay",
                                "revision": "P3",
                                "date": "2018-10-01",
                                "revisionInfo": "Minor corrections, updated for GIR rev P2.",
                                "remarks": "Some remarks if required"
                            },
                            "remarks": "Some remarks if required"
                        }
                    ],
                    "agsiGeometryFromFile": {
                        "geometryID": "GeologyGCCTop",
                        "description": "Top of GC",
                        "geometryType": "Surface",
                        "fileFormat": "LandXML",
                        "fileFormatVersion": "2.0",
                        "fileURI": "geometry/geology/GCtop.xml",
                        "filePart": "GCTop",
                        "revision": "P2",
                        "date": "2018-10-07",
                        "revisionInfo": "Updated for GIR rev P2. Additional BH from 2018 GI included.",
                        "remarks": "Some remarks if required"
                    },
                    "agsiDataPropertyFromFile": {
                        "dataID": "42f18976-7352-4f67-9a6e-df89788343a7",
                        "description": "Additional data points for top of Gotham Clay from legacy boreholes, based on points marked on plan reference xxxx in report yyyy etc",
                        "fileFormat": "XLSX",
                        "fileFormatVersion": "2019",
                        "fileURI": "data/geology/legacydata.xlsx",
                        "filePart": "GothamClay",
                        "revision": "P3",
                        "date": "2018-10-01",
                        "revisionInfo": "Minor corrections, updated for GIR rev P2.",
                        "remarks": "Some remarks if required"
                    },
                    "remarks": "Some remarks if required"
                }
            ],
            "remarks": "Some additional remarks"
        }
    ]
}
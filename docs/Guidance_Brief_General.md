# AGSi Guidance / Introduction to AGSi

## Beginners guide to object models and JSON

This is part 2 of 3 of our *Introduction to AGSi* guide.
This part discusses object models and JSON encoding in general. If you are familiar with these concepts then you may prefer to skip this page and go straight to
[part 3](./Guidance_Brief_Schema.md).

[Part 1](./Guidance_Brief_Scope.md) described what AGSi is, what it can/should be used for and some fundamental concepts.
[Part 3](./Guidance_Brief_Schema.md) will provide a tour of the AGSi schema.

*Reading time for this page is about 6 mins (reading time for all parts is 20-25 mins)*

### Object model concept

The AGSi schema is based on an object model. The schema defines what each object represents. Objects have attributes that describe the object in more detail.

!!! Note
    Strictly speaking, the documentation should refer to object *classes* in the schema, and *instances* of object classes in the data. However, one of the objectives of the documentation has been to keep the technical terminology as simple as possible, to make it easier for non-data specialists to follow. Therefore the documentation refers to both object classes and instances as *objects* and only includes the full terms when necessary for disambiguation.

Object attributes may have values assigned to them. By default these are single values but it is also possible to have arrays of values. The schema specifies what is permitted for a given attribute.

Attribute values, or arrays of values, must conform to a defined type where specified in the schema, which will be one of *string*, *numeric*, *Boolean*, *null* or *object* (as described below).

!!! Note
    In the AGSi schema an array is the same as a 'list' as all AGSi arrays are one dimensional.

Objects may be embedded (nested) within other objects. To achieve this, a *child* object becomes the value assigned to an attribute in a *parent* object. The schema defines what objects may be included for that attribute, and how many. For example, the schema may allow only one embedded object (one to one relationship) or it may allow multiple objects, within an array (one to many relationship).

!!! Note
    Those of you coming from a database and/or an AGS (factual) data format background need not feel intimidated by the concepts and terminology discussed here. In practice, objects are pretty much analogous to database tables (or AGS groups) with attributes equivalent to database table fields (AGS headings). Embedment of objects is a way of creating parent-child relationships.

Relationships between objects may also be defined using cross-reference links.
In this case a relationship is formed when the same value is provided for the specified attributes in the linked objects (like a foreign key in a database).

AGSi uses all of the above concepts.

### Understanding schema diagrams

The AGSi documentation uses diagrams to illustrate how the schema is structured. There are two basic types of diagram that you need to understand, as described below.

!!! Note
    The AGSi schema diagrams are based on
    <a href="https://github.com/ISO-TC211/UML-Best-Practices/wiki/Introduction-to-UML" target="_blank">UML class diagram methodology</a>,
    but with a few simplifications to make them more accessible to non-data specialists.

Summary AGSi UML diagrams include only object names and parent-child relationships, as illustrated by the example below. Attributes and cross-reference relationships are not shown.

<br>

![Guidance on summary UML diagram](./images/Guidance_UML_Summary.svg)

<br>

Full AGSi UML diagrams include object attributes and all relationships (within diagram scope - see note below) as illustrated by the example below.

<br>

![Guidance on full UML diagram](./images/Guidance_UML_Full.svg)

<br>

In practice, many of the 'full' diagrams provided in the documentation are a hybrid of the above as they often incorporate only a summary representation for some parts of the schema.

!!! Note
    Most diagrams are curtailed by scope, eg. only including objects from a particular group and excluding links to objects in other parts of the schema. The scope should be clearly apparent from the context or associated notes.


### Introduction to JSON

Encoding is the process by which the data, arranged in accordance with the schema, is converted to a specified format for transmittal. The term encoding is also used for the end product of this process.

AGSi has been drafted to facilitate different methods of encoding. However, at present only one method of encoding is defined, which is **JSON**.

JSON, which stands for *JavaScript Object Notation*, is a commonly used text based data-interchange format. It is defined in a standard (<a href="https://www.iso.org/standard/71616.html" target="_blank">ISO/IEC 21778</a>) and a comprehensive guide can be found
<a href="https://www.json.org/json-en.html" target="_blank">here at www.json.org</a>

JSON is simple and easy to understand, even for non-data specialists. Here is a quick run through...

#### JSON objects and arrays

JSON Comprises two basic constructs:

**1.   Object = { Name : value } pair**

``` json
{"my property name" : "its value" }
```

An object can have several properties:

``` json
{ "my property name " : "its value" ,    "another property" : "another value" }
```

**2.   Array = [ Ordered list of values ]**

This is an array:

``` json
[ "Item 1" , "Item 2" ,  "Item 3" ]
```

Arrays are one dimensional, but can be nested:

``` json
[ "Main list item 1", [ "Sublist item 1", "Sublist item 2" ], "Main list item 3" ]
```

In addition:

- objects can be embedded (nested) within other objects
- arrays can be embedded within objects
- objects can be embedded within arrays
- etc.

Confirming the syntax used:

``` json
{ "Objects use" : "curly brackets" }

[ "Arrays use" , "square brackets" ]
```

#### JSON data types

JSON recognises the following data types:

* String: must be enclosed in "double quotes"
* Number: quotes not required
* Boolean: `true` or `false`
* null: `null`
* JSON object
* JSON array

Numbers in JSON must comply with the JSON standard, whose requirements are similar to those used in most programming languages.
Numbers using scientific notation are acceptable in JSON encoding.

Date/time and similar more specific data formats are not natively
supported by JSON. However, date/time data can be provided as suitably formatted string data.

#### Other considerations

JSON data can be written in one line, or spread over many lines, and white space may be added for readability. Lines breaks and white space are ignored in parsing (unless within object names or value strings).

There are a few characters used commonly in text that are reserved characters that cannot be
directly used in JSON strings. The main ones that you need to be aware of are:

* Newline to be replaced with `\n`
* Tab to be replaced with `\t`
* Double quote to be replaced with `\"`
* Backslash to be replaced with `\\`

<a href="https://json-schema.org/" target="_blank">JSON Schema</a> provides a method for describing a schema that may be used for validation. We have developed a JSON Schema file for AGSi to facilitate such validation. This validation can include dates and other special string formats.


### JSON encoding of AGSi

So, as you can hopefully see from the above, JSON is pretty straightforward.

The rules for JSON encoding of AGSi are also simple:

- AGSi objects are represented as JSON objects, and AGSi arrays as JSON arrays.

- The AGSi schema includes nested objects. JSON encoding of AGSi also follows a nested approach, i.e. objects embedded within objects, where required by the schema.

And that is pretty much all you need to know! You will see some examples of JSON encoding of AGSi in the next section...

### Next

In [part 3](./Guidance_Brief_Schema.md) (the final part) we take you on a tour of the AGSi schema, which includes information on how an AGSi file (as JSON) is constructed.

### Version history

First formal issue, for v1.0.0 of standard, 30/11/2022.

# AGSi Guidance / Models

## Volume models - Worked examples

### Geological model using surfaces - example

An example of a very simple geological model is given in the following.
This illustrates the use of top/bottom surfaces for defining the model.

A copy of the full JSON data file described below can be found
[here](./exampledata/AGSi_Guidance_Examples.zip).


!!! Note
    In the following the JSON data is broken up in to several sections to allow commentary to be provided. To ensure that the JSON data is formatted correctly it has been necessary to include an additional "{" or "[" at the top and/or bottom of some of the sections. For the correct version of full JSON file please use the link posted above.

!!! Note
    The external files providing the surface geometry are not included in the example data.

#### Model metadata

This first section contains model metadata in the <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModel">agsiModel</a> object.


``` json
    {
      "agsiModel": [
          {
              "modelID": "1fb599ab-c040-408d-aba0-85b18bb506c2",
              "modelName": "Example simple geological model",
              "modelType": "Geological model",
              "category": "Observational",
              "domain": "Engineering geology",
              "usage": "Visualisation only",
}]}
```

#### Model boundary

This next part establishes the model boundary using an <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelBoundary">agsiModelBoundary</a>
object. In this case the boundary is a simple
rectangular box defined by limiting coordinates and a base elevation.

``` json
        {
              "agsiModelBoundary": {
                  "minX": 12000,
                  "maxX": 13000,
                  "minY": 5000,
                  "maxZ": 6000,
                  "bottomElevation": -25
              },
}
```

#### Model elements

Now for the elements that make up the model, in this case three geological units.
Each is defined by an <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelElement">agsiModelElement</a> object.

The next snippet has the data for only the first model element, which in this case represents Made Ground.

The topmost attributes identify what the model element represents.

Then, using the <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelElement#agsigeometry">*agsiGeometry*</a>
attribute, element geometry is defined by way of an embedded
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryVolFromSurfaces">agsiGeometryVolFromSurfaces</a>
object. This has attributes
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryVolFromSurfaces#agsigeometrytop">*agsiGeometryTop*</a>
and
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryVolFromSurfaces#agsigeometrybottom">*agsiGeometryBottom*</a>
defining the top and bottom surfaces geometries (use of both is not mandatory, but is recommended).
Each of these contains an embedded
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryFromFile">agsiGeometryFromFile</a>
object pointing to the data file containing the actual surface geometry (a LandXML file in this case). In this example, a relative reference link to a supporting file included as part of the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#agsi-package">AGSi package</a>.

!!! Note
    The external files providing the surface geometry are not included in the example data.


``` json
        {
            "agsiModelElement": [
                {
                    "elementID": "MG",
                    "elementName": "Made Ground",
                    "elementType": "Geological unit",
                    "geometryObject": "agsiGeometryVolFromSurfaces",
                    "agsiGeometry": {
                        "agsiGeometryTop": {
                            "description": "Top of MG",
                            "geometryType": "Surface",
                            "fileFormat": "LANDXML",
                            "fileFormatVersion": "1.2",
                            "fileURI": "geometry/geology/MGtop.xml"
                        },
                        "agsiGeometryBottom": {
                            "description": "Top of ALV / Bottom of MG",
                            "geometryType": "Surface",
                            "fileFormat": "LANDXML",
                            "fileFormatVersion": "1.2",
                            "fileURI": "geometry/geology/ALVtop.xml"
                        }
                    }
                },
]}
```

Here is the data for the next model element, for Alluvium, which follows a similar pattern.

``` json
                {
                    "elementID": "ALV",
                    "elementName": "Alluvium",
                    "elementType": "Geological unit",
                    "geometryObject": "agsiGeometryVolFromSurfaces",
                    "agsiGeometry": {
                        "agsiGeometryTop": {
                            "description": "Top of ALV / Bottom of MG",
                            "geometryType": "Surface",
                            "fileFormat": "LANDXML",
                            "fileFormatVersion": "1.2",
                            "fileURI": "geometry/geology/ALVtop.xml"
                        },
                        "agsiGeometryBottom": {
                            "description": "Top of GC / Bottom of ALV",
                            "geometryType": "Surface",
                            "fileFormat": "LANDXML",
                            "fileFormatVersion": "1.2",
                            "fileURI": "geometry/geology/GCtop.xml"
                        }
                    }
                },
```

Now the data for the final model element, for Gotham Clay. A variation here is that the bottom surface is the bottom of the model, which is a flat place at -25mOD. Therefore an <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryPlane">agsiGeometryPlane</a> object has been embedded instead of <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryFromFile">agsiGeometryFromFile</a>.  In practice, only the elevation attribute needs to be defined, as seen at the bottom of this snippet.

``` json
               {
                   "elementID": "GC",
                   "elementName": "Gotham Clay",
                   "elementType": "Geological unit",
                   "geometryObject": "agsiGeometryVolFromSurfaces",
                   "agsiGeometry": {
                       "agsiGeometryTop": {
                           "description": "Top of GC / Bottom of ALV",
                           "geometryType": "Surface",
                           "fileFormat": "LANDXML",
                           "fileFormatVersion": "1.2",
                           "fileURI": "geometry/geology/GCtop.xml"
                       },
                       "agsiGeometryBottom": {
                           "elevation": -25
                       }
                   }
               }
            ]
}]}
```

### Simple geotechnical design model using layers

Example data illustrating a simple layer model, ie. a stratigraphical column as shown below, is given in the following.

A copy of the full data file is included
[here](./exampledata/AGSi_Guidance_Examples.zip).  

![Layer model example](./images/Guidance_Model_Volume_Layers.png)

!!! Note
    Limiting valid areas and links to data/features
    and some other useful attributes that would normally benefit
    such a model have been excluded for brevity.

This example has three horizontal layers, represented by three
model elements. For each of these the geometry is defined using embedded
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryLayer">agsiGeometryLayer</a> objects
(only the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryLayer#topelevation">*topElevation*</a> and
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryLayer#bottomelevation">*bottomElevation*</a>
attributes are used).

``` json
    {
    "agsiModel": [
        {
            "modelID": "1fb599ab-c040-408d-aba0-85b18bb506c2",
            "modelName": "Example simple geotechnical design model",
            "modelType": "Geotechnical design model",
            "category": "Analytical",
            "domain": "Geotechnical",
            "usage": "Design for some specified use case",
            "agsiModelElement": [
                {
                    "elementID": "MG",
                    "elementName": "Made Ground",
                    "elementType": "Geotechnical unit",
                    "geometryObject": "agsiGeometryLayer",
                    "agsiGeometry": {
						"topElevation": 22.5,
						"bottomElevation": 19
					}
                },
                {
                    "elementID": "ALV",
                    "elementName": "Alluvium",
                    "elementType": "Geotechnical unit",
                    "geometryObject": "agsiGeometryLayer",
                    "agsiGeometry": {
						"topElevation": 19,
						"bottomElevation": 14.5
					}
                },
                {
                    "elementID": "GC",
                    "elementName": "Gotham Clay",
                    "elementType": "Geotechnical unit",
                    "geometryObject": "agsiGeometryLayer",
                    "agsiGeometry": {
						"topElevation": 14.5,
						"bottomElevation": -20
					}
                }
            ]
        }
    ]
}
```

### Version history

Minor update, for v1.0.1 of standard, June 2024:

- Minor JSON syntax errors in example boxes and files corrected (issue #181, 182)
- Links to standard updated (for change of address)

First formal issue, for v1.0.0 of standard, 30/11/2022.

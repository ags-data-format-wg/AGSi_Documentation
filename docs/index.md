# AGSi Home page

## Association of Geotechnical & Geoenvironmental Specialists transfer format for ground model and interpreted data (AGSi)

This is the home page for AGSi documentation. For full contents please browse the
navigation menu on the left. Please also read
[Structure of this documentation](#structure-of-this-documentation) below.

### What is AGSi?

AGSi is a schema and transfer format for the exchange of ground model and
interpreted data, for use in the geotechnical, geological, hydrogeological
and geo-environmental domains.

It has been created by and is maintained by the
<a href="https://www.ags.org.uk/" target="_blank">Association of Geotechnical
and Geoenvironmental Specialists (AGS)</a>,
a UK-based industry organisation.

AGSi complements the existing and well established
<a href="https://www.ags.org.uk/data-format/" target="_blank">AGS format</a>
for factual ground investigation data.

**The [Introduction to AGSi](./Guidance_Brief_Scope.md) guidance is the ideal place to start for those new to AGSi.
This short guide (total read time 20-25 minutes) explains what AGSi is and how it works.**


### Status

AGSi is now live! After a couple of years of beta releases, version 1.0.0 was officially launched at the
<a href="https://www.ags.org.uk/item/the-geotechnical-data-conference/" target="_blank">AGS Geotechnical Data Conference</a>
on 1 December 2022. The first (very minor) update, version 1.0.1, was issued in June 2024.

AGS have published a
<a href="https://www.ags.org.uk/content/uploads/2022/05/AGSi_roadmap_v1-0-1.pdf" target="_blank">roadmap</a>
which sets out the vision for the implementation and further development of AGSi.

### Comments and feedback

AGS welcomes comment, feedback and suggestions on the implementation and further development of AGSi from industry practitioners and software developers.
Please use the
<a href="https://www.ags.org.uk/data-format/dwqa-questions/" target="_blank">discussion forum on the AGS website</a>.

If you are not able to use the discussion forum (i.e. not an AGS member) then you may use the <a href="https://www.ags.org.uk/data-format/agsi-ground-model/" target="_blank">enquiry form on the AGS website</a> instead.

If you would like to join our **early adopters community**, and/or would like to get involved in the development of AGSi going forward, then please use one of the above methods to contact us and we will get back to you.


### Structure of this documentation

After this standalone **Home page**, the documentation is split as follows:

* The **Standard** section comprises the formal definition of AGSi,
both the schema and its encoding, rules for its intended usage and lists of standard codes
and terms recommended for use within AGSi.
The Standard is version controlled - use selector at top of page to toggle between versions.

* The **Guidance** provides further information about how AGSi can be used in practice,
including examples.  It is anticipated that we will update and improve the
guidance from time to time, in response to user feedback.
The Guidance is separately version controlled at page level. It should be applicable to the current version of the standard unless otherwise stated.

This Home page is considered to be part of the Guidance.


### Version history (for this page only)

Minor update June 2024: reference to v1.0.1 added.

Minor update 14/12/22 to confirm that v1.0.0 was launched on 1 December 2022!

First formal issue, for v1.0.0 of standard, 30/11/2022.
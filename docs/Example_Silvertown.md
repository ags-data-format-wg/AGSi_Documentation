# AGSi Guidance / Example files

## Silvertown tunnel

This project is part of the infrastructure improvement programme and aims to provide new road crossings of the River Thames addressing congestion and delays in east and south east London (UK).

![Data example - input schedule](./exampledata/Silvertown/TfL.png)
This sample dataset has been provided by kind permission of Transport for London (TfL) for the purpose of AGSi developments. TfL is not liable for any errors or inaccuracy of its content.

Silvertown tunnel design model with background mapping:

![Data example - input schedule](./images/Example_Silvertown_BingMap.png)

Silvertown tunnel design model with TIN surfaces and cross sections:

<img src="../exampledata/Silvertown/Visuals/SilvertownGroundModelSpin-LowRes.gif" width=50% height=50%><img src="../exampledata/Silvertown/Visuals/SectionBitmaps/GeologicalSectionNB%26SBAlignments2D.png" width=50% height=50%>

The example AGSi packages were created from the report and design model. 3D model packages, which contain data needed to rebuild sections, boreholes as observations and TIN surfaces together with summary of geotechnical parameters are bundled based on surface geometry representation format. Geometries are represented by STL, DXF and WKT file formats and are saved in external files inside the corresponding package. 2D agsi package files are generated to demonstrate data in 2D coordinate system for each of the two cross sections where geometries are represented by WKT and DXF file formats.

Silvertown tunnel model with displayed parameters imported from example AGSi package:

![Data example - input schedule](./exampledata/Silvertown/Visuals/04%20-%20London%20Clay.png)

Geotechnical parameters displayed in AGSi JSON file matching the highlighted surface from the image above:

![Data example - input schedule](./images/Example_Silvertown_AGSi.png)

!!! Note 
    The example data is constantly improved and expanded feature wise. This page will be updated with any significant feature changes and the latest set of the Silvertown tunnel example files will always be available from the download below. Example files last updated on 01st of December 2022.

Full example data files for both the 3D and 2D models can be downloaded from:

- [2D sections in wkt format-1](./exampledata/Silvertown/Silvertown2D_wkt_1.agsi)
- [2D sections in dxf format-1](./exampledata/Silvertown/Silvertown2D_dxf_1.agsi)
- [2D sections in wkt format-2](./exampledata/Silvertown/Silvertown2D_wkt_3.agsi)
- [2D sections in dxf format-2](./exampledata/Silvertown/Silvertown2D_dxf_3.agsi)
- [3D model with stl surfaces and wkt profiles](./exampledata/Silvertown/Silvertown3D_stl.agsi)
- [3D model with wkt surfaces and profiles](./exampledata/Silvertown/Silvertown3D_wkt.agsi)
- [3D model with dxf surfaces and profiles](./exampledata/Silvertown/Silvertown3D_dxf.agsi)

!!! Note
    AGSi is packaged as a compressed .zip file, with .agsi file extension.
    Various methods could be adopted to view the content, some are listed below:

    - Using windows explorer: rename .agsi extension to .zip will allow you to view.
    - Using 7‐zip: right click on file to extract or view.
    - Python: open‐source tools are available

